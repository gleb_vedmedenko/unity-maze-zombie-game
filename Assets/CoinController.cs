﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour {

    public Animator anim;
	// Use this for initialization
	void Start () {

        anim = GetComponent<Animator>();
	}
	
	public void GetCoin()
    {
        anim.SetTrigger("GetCoin");
    }
}

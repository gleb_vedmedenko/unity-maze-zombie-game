﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    private XMLMessenger messanger = new XMLMessenger();
    private ScoreContainer cont = new ScoreContainer();
    public delegate void SpawnZombie();
    public delegate void SpawnMummy();
    public delegate void IncreaseSpeed();
    public static event IncreaseSpeed increaseSpeed;
    public static event SpawnMummy spawnMummy;
    public static event SpawnZombie spawnZombie;
    public  int score = 0;
    private float time;
    // Use this for initialization
    void Start () {

        time = 0f;
    }
   
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        FinishGame();
        
    }
    
    private void FinishGame()
    {
        if(Input.GetKey(KeyCode.Escape))
        {
            cont.score = 1155.ToString();
            cont.time = 155.ToString();
            cont.playerName = "Name";
            cont.exitCause = "Ecs";
            System.DateTime d = System.DateTime.Now;
            string s = string.Format("{0:g}", d);
            cont.date = s;
            messanger.xmlWrite(Application.dataPath + "//Config.xml", cont);
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
    }
    private void FinishGame(string cause)
    {
            cont.playerName = PlayerPrefs.GetString("Name");
            cont.time = time.ToString();
            cont.score = score.ToString();
            cont.exitCause = cause;
            System.DateTime d = System.DateTime.Now;
            string s = string.Format("{0:g}", d);
            cont.date = s;
            messanger.xmlWrite(Application.dataPath + "//Config.xml", cont);
          //  UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        switch(other.gameObject.tag)
        {
            case "Enemy":
                FinishGame("Zombie");
                break;
            case "Coin":
                Animator anim = other.gameObject.GetComponent<Animator>();
                anim.applyRootMotion = false;
                anim.SetTrigger("GetCoin");
                score++;
                Controller.coinsInMaP--;
                Debug.Log(Controller.coinsInMaP);
                if (score == Controller.nextZombie)
                {
                    Debug.Log(score + "TRUE");
                    spawnZombie();
                }
                    
                if (score == Controller.mummySpawn)
                    spawnMummy();
                if (score == Controller.pathFindingCoin)
                    Controller.isFindPlayer = true;
                if (score >= Controller.increaseSpeedCoins)
                {

                    increaseSpeed();
                }
                break;
            case "Mummy":
                score = 0;
                FinishGame("Mummy");
                break;
        }
        
            
    }
    void OnTriggerStay2D(Collider2D other)
    {
        if(other.gameObject.tag == "ZombieRadius")
            other.gameObject.GetComponentInParent<Z_Controller>().Attack(transform.position);
        if (other.gameObject.tag == "MummyRadius")
            other.gameObject.GetComponentInParent<MummyController>().Attack(transform.position);
    }
}

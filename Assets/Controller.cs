﻿
public static class Controller {
    public static float zombieSpeed = 1f;
    public static float mummySpeed = 2f;
    public static int maxCoins = 10;
    public static int increaseSpeedCoins = 20;
    public static int coinsInMaP = 0;
    public static int nextZombie = 5;
    public static int mummySpawn = 10;
    public static int pathFindingCoin = 20;
    public static bool isFindPlayer = false;
    

    public static float IncreaseSpeed(ref float speed,float percent)
    {
        float plusSpeed = speed + ((speed/100) * percent);
        return plusSpeed;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScoreView : MonoBehaviour {

    private GameObject obj;
    XMLMessenger messanger = new XMLMessenger();
    private float step = 70f;
    private Vector2 pos = Vector2.zero;
	void Start()
    {


        Debug.Log(gameObject.GetComponent<RectTransform>().sizeDelta.x);
        Debug.Log(gameObject.GetComponent<RectTransform>().rect.height);
        SetScore(messanger.xmlRead(Application.dataPath + "\\Config.xml"));
       
    }
    void SetScore(List<ScoreContainer> score)
    {
        score.Reverse();
        for (int i = 0; i < score.Count; i++)
        {
            string result_str = "";
            result_str = (i + 1).ToString() + ". " + score[i].playerName + " " + score[i].score + " " +
                score[i].time + " " + score[i].exitCause + " " + score[i].date;
               
            
            AddNewScore(result_str);

        }
    }
    void AddNewScore(string scoreLine)
    {
        GameObject obj = new GameObject();
        RectTransform rect = obj.AddComponent<RectTransform>();
        Text text = obj.AddComponent<Text>();
        Font ArialFont = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");
        text.font = ArialFont;
        text.fontSize = 20;
        rect.SetParent(gameObject.GetComponent<RectTransform>());
        
        if (pos == Vector2.zero)
        {
            
            rect.anchorMin = new Vector2(0.5f, 1f);
            rect.anchorMax = new Vector2(0.5f, 1f);
            rect.pivot = new Vector2(0.5f, 0.5f);
            rect.sizeDelta = new Vector2(645f,50f);
            rect.localPosition = new Vector2(0f, -50f);
            pos = rect.localPosition;
        }
        else
        {
            pos = new Vector2(0f, pos.y - step);
            rect.anchorMin = new Vector2(0.5f, 1f);
            rect.anchorMax = new Vector2(0.5f, 1f);
            rect.pivot = new Vector2(0.5f, 0.5f);
            rect.sizeDelta = new Vector2(645f, 50f);
            rect.localPosition = pos;
        }

        // string tex = XMLMessenger.Load(Application.dataPath + "\\Config.xml").score.ToString();
        text.text = scoreLine;

    }
}

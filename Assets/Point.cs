﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point  {

    public int x;
    public int y;
    private int f;
    private int g;
    private int h;
    private Vector2 position;
    public int X { get { return x; } set { x = value; } }
    public int Y { get { return y; } set { y = value; } }
    public int G { get { return g; } set { g = value; } }
    public int H { get { return h; } set { h = value; } }
    public int F { get { return G  + H; } set { f = value; } }
    public Vector2 Position {  get { return position; } set { position = value; } }
    public Point parent;
    public bool isWalktable = true;
    public void setProprietes(int x,int y,Vector2 position)
    {
        X = x;
        Y = y;
        Position = position;

    }
    public void setProprietes(int x, int y)
    {
        X = x;
        Y = y;
    }
}

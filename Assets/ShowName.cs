﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowName : MonoBehaviour {


	public void ShowNamePanel()
    {
        GameObject obj = GameObject.Find("NamePanel");
        obj.GetComponent<Animation>().Play("ChangeName");
    }
}

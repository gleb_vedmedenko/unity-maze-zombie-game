﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Z_Controller : MonoBehaviour {
    private Animator anim;
    private float speed = 1f;
    private NewStar star;
    private Vector2 old;
    private SpriteRenderer sprite;
    private PlayerController playerController;
    private int score;
    void Start()
    {
        anim = GetComponent<Animator>();
        star = GetComponent<NewStar>();
        sprite = GetComponent<SpriteRenderer>();
        star.speed = Controller.zombieSpeed;
        playerController =GameObject.Find("Player").GetComponent<PlayerController>();
        score = playerController.score;
    }
    private void SideMove()
    {
        if (old.x < transform.position.x)
        {
            old = transform.position;
            anim.SetTrigger("Move");
            sprite.flipX = true;
        }
        else if (old.x > transform.position.x)
        {
            old = transform.position;
            anim.SetTrigger("Move");
            sprite.flipX = false;

        }
        else if (old.y != transform.position.y)
            anim.SetTrigger("Move");
        else
            anim.SetTrigger("Stay");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void FixedUpdate()
    {

        SideMove();
    }
    public void Attack(Vector3 target)
    {
        if(star.isSameTile())
        {
            
            anim.SetTrigger("Attack");
            transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
        }
        
    }
    private void IncreaseSpeed()
    {
        speed = speed + ((speed / 100) * 5);
        star.speed = speed;
        Debug.Log(speed);
    }
    void OnEnable()
    {
        PlayerController.increaseSpeed += IncreaseSpeed;
        
    }
    void OnDisable()
    {
        PlayerController.increaseSpeed -= IncreaseSpeed;

    }

}

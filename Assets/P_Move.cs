﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P_Move : MonoBehaviour
{
    private SpriteRenderer sprite;
    private Animator anim;
    public float speed = 2.5f;
    public static Point currentPos;

    // Use this for initialization
    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        currentPos = MapGen.map[0];
        transform.position = currentPos.Position;
    }

    void FixedUpdate()
    {
        Move();
    }
    void Move()
    {
        if (Input.GetKey(KeyCode.DownArrow))
            MoveDiretion(0f, -speed);
        if (Input.GetKey(KeyCode.UpArrow))
            MoveDiretion(0f, speed);
        if (Input.GetKey(KeyCode.RightArrow))
            MoveDiretion(speed, 0f);
        if (Input.GetKey(KeyCode.LeftArrow))
            MoveDiretion(-speed, 0f);
        if (!Input.anyKey)
        {
            anim.ResetTrigger("Move");
            anim.SetTrigger("Stay");
        }
            
        
    }
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.name == "Ground(Clone)")
        {
            currentPos = coll.gameObject.GetComponent<Location>().tilePoint;
            
        }
    }
    void MoveDiretion(float x,float y)
    {
        anim.SetTrigger("Move");
        transform.Translate(new Vector2(x, y) * Time.deltaTime);
        if (x > 0)
            sprite.flipX = false;
        else
            sprite.flipX = true;
    }
    
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackScore : MonoBehaviour {
    private GameObject obj;
    // Use this for initialization

    public void Play()
    {
        obj = GameObject.Find("Scroll View");
        try
        {
            obj.GetComponent<Animation>().Play("ScoreBack");
        }
        catch(System.Exception e)
        {
            Debug.Log(e + "\nScroll View not found, object name should be 'Scroll View'");
        }
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class NewStar : MonoBehaviour {
    public Point startPos;
    public Point target;
    private Point tempTarget = null;
    private Point currentPos = null;
    List<Point> openList = new List<Point>();
    List<Point> closedList = new List<Point>();
    List<Point> posCurrents = new List<Point>();
    List<Point> posTemp = new List<Point>();
    Point finalNode = null;
    GameObject enemy_obj;
    public float timeChange = 7f;
    private float tempTime;
    public float speed;

    int i = 0;
    void Start()
    {
        if(Controller.isFindPlayer)
            target = P_Move.currentPos;
        else
            target = RandomPoint();
        tempTime = timeChange;
        tempTarget = target;
        startPos.G = 0;
        startPos.H = ComputeHScore(startPos.X, startPos.Y, target.X, target.Y);
        startPos.parent = null;
        openList.Add(startPos);

        while (openList.Count > 0)
        {
            
            Search(target, ref openList,ref posCurrents);
        }

        //foreach(Point p in posCurrents)
        //{
        //    Debug.Log(p.X + "" + p.Y);
        //}
    }
    private Point RandomPoint()
    {
        int rand = Random.Range(0, MapGen.walkableMap.Count - 1);
        Point p = MapGen.walkableMap[rand];
        return p;
    }
    void Update()
    {
        if(!Controller.isFindPlayer)
        PathTimer();
        if (TargetChange())
        {
            while (openList.Count > 0)
            {
                Search(target, ref openList, ref posCurrents);
            }
        }
    }
    public bool isSameTile()
    {
        if (target.Position == currentPos.Position)
        {
            posCurrents.Clear();
            openList.Clear();
            closedList.Clear();
            return true;
        }
        else
            return false;
    }
    private void PathTimer()
    {
        timeChange -= Time.deltaTime;
        if(timeChange <= 0)
        {
            tempTarget= RandomPoint();
            timeChange = tempTime;
        }
    }
    private bool TargetChange()
    {
        if(Controller.isFindPlayer)
        {
            if (target != P_Move.currentPos)
            {
                closedList.Clear();
                openList.Clear();
                target = P_Move.currentPos;
                startPos = currentPos;
                startPos.G = 0;
                startPos.H = ComputeHScore(startPos.X, startPos.Y, target.X, target.Y);
                startPos.parent = null;
                openList.Add(startPos);
                posCurrents.Clear();
                i = 0;
                return true;
            }
        }
        else
        {
            if(target.Position != tempTarget.Position)
            {
                closedList.Clear();
                openList.Clear();
                target = tempTarget;
                startPos = currentPos;
                startPos.G = 0;
                startPos.H = ComputeHScore(startPos.X, startPos.Y, target.X, target.Y);
                startPos.parent = null;
                openList.Add(startPos);
                posCurrents.Clear();
                i = 0;
                return true;
            }
        }
        return false;
        //if (target.Position != P_Move.currentPos.Position)
        //{
        //    target = P_Move.currentPos;
        //    posCurrents.Add(target);
        //    Point p = new Point();
        //    p.setProprietes(P_Move.currentPos.x, P_Move.currentPos.y, P_Move.currentPos.Position);
        //    posTemp.Add(p);
        //    if (posTemp.Count > 5)
        //    {


        //        return true;
        //    }

        //}
        //return false;
    }
    void FixedUpdate()
    {
      
        
        if (posCurrents.Count > 0)
        {
            MoveEnemy(ref posCurrents);
        }

    }
    private void Search(Point target, ref List<Point> openList,ref List<Point> pos)
    {
        
            var lowest = openList.OrderBy(p => p.F).First();

            if (lowest.Position == target.Position)
            {

                openList.Clear();
                closedList.Clear();
                pos.Add(lowest);
                GetPath(lowest,ref pos);
                
            }

            openList.Remove(lowest);
            closedList.Add(lowest);
            var adjacentPoints = GetAdjacentSquares(lowest.X, lowest.Y, lowest, target, MapGen.map);
        
        foreach (var adjacentSquare in adjacentPoints)
            {
            
            if (closedList.Count(p => p.Position == adjacentSquare.Position) > 0)
                    continue;
                var openNode = openList.FirstOrDefault(p => p.Position == adjacentSquare.Position);

            if (openNode == null)
                {
                    adjacentSquare.parent = lowest;
                    adjacentSquare.G = lowest.G + 1;
                    adjacentSquare.H = ComputeHScore(adjacentSquare.X, adjacentSquare.Y, target.X, target.Y);
                    openList.Add(adjacentSquare);
                }

                //else if (openNode.G > adjacentSquare.G)
                //{

                //    openNode.parent = lowest;
                //    openNode.G = adjacentSquare.G;
                //}

            }
            
        }
        
    
    //public static void Add(ref List<Point> points, int x, int y)
    //{
    //    Point p = new Point();
    //    p.setProprietes(x, y);
    //    points.Add(p);
    //}
    public void GetPath(Point p,ref List<Point> pos)
    {

        Point next = p.parent;
        if (next != null)
        {
         
            pos.Insert(0, next);
            GetPath(next,ref pos);
        }
        
        
    }
    public static void SetParams(Point p, Point parent, Point target, ref List<Point> pList)
    {
        Point new_p = new Point();
        new_p.X = p.X;
        new_p.Y = p.Y;
        new_p.Position = p.Position;
        new_p.parent = parent;
        new_p.G = parent.G + 1;
        new_p.H = ComputeHScore(p.X, p.Y, target.X, target.Y);
        pList.Add(new_p);
    }
    static int ComputeHScore(int x, int y, int targetX, int targetY)
    {
        return System.Math.Abs(targetX - x) + System.Math.Abs(targetY - y);
    }
    //private static List<Point> GetPath(Point p)
    //{
    //    var res = new List<Point>();
    //    var current = p.parent;

    //    if(current != null)
    //    {
    //        res.Add(current);
    //        current = current.parent;
    //    }
    //    res.Reverse();

    //    return res;
    //}
    static List<Point> GetAdjacentSquares(int x, int y, Point current, Point goal, List<Point> map)
    {
        List<Point> adjPoint = new List<Point>();

        foreach (Point p in map)
        {
            Point new_p = new Point();
            new_p.setProprietes(p.X, p.Y, p.Position);
            new_p.isWalktable = p.isWalktable;
            if (new_p.isWalktable)
            {
                if (new_p.X == x && new_p.Y == y - 1)
                    adjPoint.Add(new_p);
                if (new_p.X == x && new_p.Y == y + 1)
                    adjPoint.Add(new_p);
                if (new_p.X == x - 1 && new_p.Y == y)
                    adjPoint.Add(new_p);
                if (new_p.X == x + 1 && new_p.Y == y)
                    adjPoint.Add(new_p);
            }
        }

        return adjPoint;
    }
    void MoveEnemy(ref List<Point> pos)
    {
        if (i < pos.Count)
        {
            transform.position = Vector2.MoveTowards(transform.position, pos[i].Position, speed * Time.deltaTime);
            if ((Vector2)transform.position == pos[i].Position && i < pos.Count - 1)
            {
                i++;
            }
        }
    }
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.name == "Ground(Clone)")
        {
            
            currentPos = coll.gameObject.GetComponent<Location>().tilePoint;
            
        }
    }
    void OnDisable()
    {
        Destroy(this);
    }
}

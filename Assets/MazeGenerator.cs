﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MazeGenerator : MonoBehaviour {
    
    public static int step_x = 0;
    public static List<MazeLine> lines = new List<MazeLine>();
    
    private MazeLine current = null;
   
	// Use this for initialization
	void Awake () {
        lines.Clear();
    }

	// Update is called once per frame
	void Update () {
		
	}
    
  public  void MazeGen(int _x,int _y)
    {
        FirstLine(_x);
      
        for (int y = 1; y < _y; y++)
        {
            if (y < _y - 1)
            {
                current = nextLine(current, _x);
                Retransform(current);
                current.setPacks(current.getTiles());
                current.setDownBoundary();
                current.printf();
                lines.Add(current);
            }
            else
            {
                current = LastLine(current);
                LastLineTransform(current);
                lines.Add(current);
                Debug.Log("LAAAAAAAAST");
                Debug.Log(current.packs.Count);
            }
              
        }
        
    }
    public void FirstLine(int x)
    {
        current = new MazeLine();
        current.setTiles(x);
        current.setPacks();
        current.setDownBoundary();
        current.printf();
        lines.Add(current);
        
    }
    public static bool isBoundary(float threshold)
    {
        float rand = Random.Range(0f, 6.5f);
        if (rand > threshold)
            return true;
        else return false;
    }
    private MazeLine nextLine(MazeLine line,int x)
    {
        MazeLine new_line = new MazeLine();
        
        foreach(Pack p in line.packs)
        {
            foreach(Tile t in p.tiles)
            {
                Tile tile = new Tile();
                tile.currentX = t.currentX;
                tile.pack_value = t.pack_value;
                tile.isBoundaryDown = t.isBoundaryDown;
                tile.isBoundaryRight = t.isBoundaryRight;
                new_line.tilelist.Add(tile);
            }
        }
        return new_line;
    }
    private void Retransform(MazeLine line)
    {         
        foreach (Tile t in line.tilelist) 
            t.deleteBoundary();  

    }
    private MazeLine LastLine(MazeLine line)
    {
        MazeLine new_line = new MazeLine();
       
        foreach (Pack p in line.packs)
        {
            foreach (Tile t in p.tiles)
            {

                Tile tile = new Tile();
                tile.currentX = t.currentX;
                tile.pack_value = t.pack_value;
                tile.isBoundaryDown = t.isBoundaryDown;
                tile.isBoundaryRight = t.isBoundaryRight;
                new_line.tilelist.Add(tile);
            }
        }
        Debug.Log(new_line.tilelist.Count);
        return new_line;
    }
    private void LastLineTransform(MazeLine line)
    {
        Pack p = new Pack(line.tilelist[0]);
        for (int i = 0; i < line.tilelist.Count; i++)
        {
            //line.tilelist[i].isBoundaryRight = false;
            //line.tilelist[i].isBoundaryDown = true;
            //Debug.Log(line.tilelist[i].pack_value);
            line.tilelist[i].isBoundaryDown = true;
            if (i < line.tilelist.Count - 1)
            {

                if (line.tilelist[i].pack_value == line.tilelist[i + 1].pack_value)
                {
                    p.Add(line.tilelist[i]);
                    continue;
                }
                else
                    line.tilelist[i].isBoundaryRight = false;

            }

            p.Add(line.tilelist[i]);


        }
        line.packs.Add(p); 
    }
}
public class MazeLine
{
    public List<Tile> tilelist = new List<Tile>();
    public List<Pack> packs = new List<Pack>();
    public List<Pack> PackList { get { return packs; } }
    private Pack currentPack = null;
    
    public void setTiles(int x)
    {
        for (int i = 0; i < x; i++)
        {
            Tile tile = new Tile();
            tile.currentX = MazeGenerator.step_x;
            
            MazeGenerator.step_x++;
            tilelist.Add(tile);
        }   
    }
    public List<Tile> getTiles()
    {
        return tilelist;
    }
    public void setPacks()
    {
        currentPack = new Pack(tilelist[0]);
        for (int i = 0; i < tilelist.Count; i++)
        {
            if (currentPack == null)
                currentPack = new Pack(tilelist[i]);
            if (MazeGenerator.isBoundary(3))
            {
                if( i < tilelist.Count - 1)
                {
                    tilelist[i].isBoundaryRight = true;
                    currentPack.Add(tilelist[i]);
                    packs.Add(currentPack);
                    currentPack = null;
                }
                if (i  == tilelist.Count- 1)
                {
                    tilelist[i].isBoundaryRight = true;
                    currentPack.Add(tilelist[i]);
                    packs.Add(currentPack);
                    currentPack = null;
                    break;
                }
            }
            else
            {
                currentPack.Add(tilelist[i]);
                if (i == tilelist.Count- 1)
                    packs.Add(currentPack);
            }
        }
    }
    public void setPacks(List<Tile> tiles)
    {
        currentPack = new Pack(tiles[0]);
        packs.Clear();
      //  currentPack = new Pack(tiles[0]);
        for (int i = 0; i < tiles.Count; i++)
            {
                
                if (currentPack == null)
                    currentPack = new Pack(tiles[i]);
                if (MazeGenerator.isBoundary(3))
                {
                    if (i < tiles.Count - 1)
                    {
                        tiles[i].isBoundaryRight = true;
                        currentPack.Add(tiles[i]);
                        packs.Add(currentPack);
                        currentPack = null;
                    }
                    else if (i == tiles.Count - 1)
                    {
                        tiles[i].isBoundaryRight = true;
                        currentPack.Add(tiles[i]);
                        packs.Add(currentPack);
                        currentPack = null;
                        break;
                    }

                }
                
                else
                 {
                if (i < tilelist.Count - 1 && (tilelist[i].pack_value == tilelist[i + 1].pack_value))
                {
                    tilelist[i].isBoundaryRight = true;
                    currentPack.Add(tiles[i]);
                    packs.Add(currentPack);
                    currentPack = null;
                    continue;
                }
                if (i == tilelist.Count - 1)
                {
                    currentPack.Add(tiles[i]);
                    packs.Add(currentPack);
                    currentPack = null;
                    break;
                }
                  
                  currentPack.Add(tiles[i]);
                    }
            }
    }
    public void setDownBoundary()
    {
        foreach(Pack p in packs)
        {
            p.setBoundaryDown();   
        }
    }
    public void printf()
    {
        Debug.Log("PACK COUNT " + packs.Count);
        foreach(Pack p in packs)
        {
            Debug.Log("------------");
            Debug.Log("value " + p.value);
            p.printTile();
        }
    }
}
public class Tile
{
    public int currentX;
    public int pack_value;
    public bool isBoundaryDown;
    public bool isBoundaryRight;
    public void deleteBoundary()
    {
        if (isBoundaryRight)
            isBoundaryRight = false;
        else if (isBoundaryDown)
        {
            isBoundaryDown = false;
            currentX = MazeGenerator.step_x;
            MazeGenerator.step_x++;
        }
    }
}
public class Pack
{
    
    public int value;
    public List<Tile> tiles = new List<Tile>();
    public bool hasBoundaryDown;
    public Pack(Tile t)
    {
        value = t.currentX;
    }
    public void Add(Tile t)
    {
        t.pack_value = value;
        tiles.Add(t);
    }
    public void setBoundaryDown()
    {
        int index = 1;
        for(int i =0;i< tiles.Count;i++)
        {

            if (tiles.Count == 1)
            {
                tiles[i].isBoundaryDown = false;
                break;
            }
            else if (MazeGenerator.isBoundary(2))
            {
                if (index < tiles.Count)
                {
                    tiles[i].isBoundaryDown = true;
                    hasBoundaryDown = true;
                    index++;
                }
                else
                    tiles[i].isBoundaryDown = false;
                       
            }
          
            }
        if (tiles.All(s => s.isBoundaryDown))
            tiles.Last().isBoundaryDown = false;
    }
    public void printTile()
    {
        foreach(Tile t in tiles)
        {
            Debug.Log("Tile x " + t.pack_value + " Has right b " + t.isBoundaryRight + " has down b " + t.isBoundaryDown);   
        }
    }

      
}
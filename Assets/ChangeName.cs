﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeName : MonoBehaviour {

	
    public void EndEdit()
    {
        GameObject obj = GameObject.Find("NameText");
        string s = obj.GetComponent<UnityEngine.UI.Text>().text;
        PlayerPrefs.SetString("Name", s);
    }
}

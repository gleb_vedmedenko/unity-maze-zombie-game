﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NamePanelBack : MonoBehaviour {

    public void ShowNamePanelBack()
    {
        GameObject obj = GameObject.Find("NamePanel");
        obj.GetComponent<Animation>().Play("NamePanelBack");
    }
}

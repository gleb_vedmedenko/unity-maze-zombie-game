﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MummyController : MonoBehaviour {

    private Animator anim;
    private Vector2 old;
    private SpriteRenderer sprite;
    private NewStar star;
    private PlayerController playerController;
    private float speed = Controller.mummySpeed;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        old = transform.position;
        sprite = GetComponent<SpriteRenderer>();
        star = GetComponent<NewStar>();
        star.speed = speed;
        playerController = GameObject.Find("Player").GetComponent<PlayerController>();
        IncreaseSpeed();
	}
    private void SideMove()
    {
        if (old.x < transform.position.x)
        {
            old = transform.position;
            anim.SetTrigger("MummyGo");
            sprite.flipX = true;
        }
        else if (old.x > transform.position.x)
        {
            old = transform.position;
            anim.SetTrigger("MummyGo");
            sprite.flipX = false;

        }
        else if (old.y != transform.position.y)
            anim.SetTrigger("MummyGo");
        else
            anim.SetTrigger("MummyStay");
    }
    public void Attack(Vector3 target)
    {
        if (star.isSameTile())
        {

            anim.SetTrigger("MummyAttack");
            transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
        }

    }
    private void IncreaseSpeed()
    {
        speed = speed + ((speed / 100) * 5);
        star.speed = speed;
        Debug.Log(speed);
    }
    void OnEnable()
    {
        PlayerController.increaseSpeed += IncreaseSpeed;

    }
    void OnDisable()
    {
        PlayerController.increaseSpeed -= IncreaseSpeed;
    }
    // Update is called once per frame
    void FixedUpdate () {
        SideMove();
	}
}

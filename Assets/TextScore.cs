﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;


public class TextScore : MonoBehaviour {

    private Text text;
    private PlayerController control;
	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
        GameObject player = GameObject.Find("Player");
        if (player == null)
            throw new System.Exception("Player Not Found");
        control = player.GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
        text.text = "Score: " + control.score.ToString();
	}
}

﻿
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Xml.Serialization;

//[XmlRoot ("Score")]
public class XMLMessenger  {
    
    
    public XmlDocument xmlCreate(string path)
    {
        XmlTextWriter writter = new XmlTextWriter(path, System.Text.Encoding.UTF8);
        writter.WriteStartDocument();
        writter.WriteStartElement("head");
        writter.WriteEndElement();
        writter.Close();
        XmlDocument doc = new XmlDocument();
        doc.Load(path);
        return doc;

    }
    public XmlDocument GetXFile(string path)
    {
        XmlDocument doc = new XmlDocument();
        doc.Load(path);
        return doc;
    }
    public void xmlWrite(string path, ScoreContainer data)
    {
        XmlDocument doc;
        if (!File.Exists(path))
            doc = xmlCreate(path);
        else
            doc = GetXFile(path);
        
        XmlNode user = doc.CreateElement("User");
        XmlNode score = doc.CreateElement("score");
        XmlNode exitCause = doc.CreateElement("exit-cause");
        XmlNode time = doc.CreateElement("time");
        XmlNode date = doc.CreateElement("date");

        doc.DocumentElement.AppendChild(user);

        XmlAttribute name = doc.CreateAttribute("name");
        name.Value = data.playerName;
        user.Attributes.Append(name);
        score.InnerText = data.score;
        exitCause.InnerText = data.exitCause;
        time.InnerText = data.time;
        date.InnerText = data.date;

        user.AppendChild(score);
        user.AppendChild(exitCause);
        user.AppendChild(time);
        user.AppendChild(date);
        doc.Save(path);


    }
    
    public List<ScoreContainer> xmlRead(string path)
    {

        XDocument docs;
        List<ScoreContainer> lines = new List<ScoreContainer>();
        try
        {
            docs = XDocument.Load(path);
            foreach (XElement element in docs.Element("head").Descendants("User"))
            {
                
                UnityEngine.Debug.Log("File not found ");
                ScoreContainer container = new ScoreContainer();
                container.playerName = element.FirstAttribute.Name + " " + element.FirstAttribute.Value;
                foreach (XElement elem in element.Descendants())
                {
                    switch(elem.Name.ToString())
                    {
                        case "score":
                            container.score = elem.Name + ": " + elem.Value;
                            break;
                        case "exit-cause":
                            container.exitCause = elem.Name + ": " + elem.Value;
                            break;
                        case "time":
                            container.time = elem.Name + ": " + elem.Value;
                            break;
                        case "date":
                            container.date = elem.Name + ": " + elem.Value;
                             break;
                        
                    }
                }
                lines.Add(container);
            }


        }
        catch (FileNotFoundException ex)
        {
            UnityEngine.Debug.Log("File not found " + " " + ex);
        }

        return lines;
        
    }
    
}

   
public struct ScoreContainer
{
   public string playerName;
   public string score;
   public string time;
   public string exitCause;
   public string date;
   
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Spawner : MonoBehaviour {

    [SerializeField]
    private GameObject enemy;
    [SerializeField]
    private GameObject mummy;
    [SerializeField]
    private GameObject coin;

    
    
    private PlayerController playerController;
    private int currentCoins;
    // Use this for initialization

    void Start () {
        GameObject obj = GameObject.Find("Player");
        if (obj == null)
            throw new System.Exception("Player not found");
        playerController = obj.GetComponent<PlayerController>();
        
        currentCoins = 0;
        StartCoroutine(SpawnCoins());
        SpawnZombie();
    }
	
    void OnEnable()
    {
        PlayerController.spawnZombie += SpawnZombie;
        PlayerController.spawnMummy += SpawnMummy;
    }
    void OnDisable()
    {
        PlayerController.spawnZombie -= SpawnZombie;
        PlayerController.spawnMummy -= SpawnMummy;
    }
    
    public void  SpawnZombie()
    {
        
            GameObject new_enemy = Instantiate(enemy);
            int rand = Random.Range(0, MapGen.walkableMap.Count - 1);
            Point startPos = MapGen.walkableMap[rand];
            new_enemy.transform.position = startPos.Position;
            new_enemy.GetComponent<NewStar>().startPos = startPos;
            
    }
    public void SpawnMummy()
    {
        
            GameObject new_mummy = Instantiate(mummy);
            int rand_map = Random.Range(0, MapGen.walkableMap.Count - 1);
            Point start = MapGen.walkableMap[rand_map];
            new_mummy.transform.position = start.Position;
            new_mummy.GetComponent<NewStar>().startPos = start;
        
    }

    IEnumerator SpawnCoins()
    {
        while(true)
        {
            if(Controller.coinsInMaP < Controller.maxCoins)
            {
                GameObject new_coin = Instantiate(coin);
                int rand = Random.Range(0, MapGen.walkableMap.Count - 1);
                Point startPos = MapGen.walkableMap[rand];
                new_coin.transform.position = startPos.Position;
                Controller.coinsInMaP++;
            }
            yield return new WaitForSeconds(10f);
        }
    }
}

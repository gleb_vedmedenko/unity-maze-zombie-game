﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowScore : MonoBehaviour {
    private GameObject obj;
	// Use this for initialization
	
    public void Show()
    {
        obj = GameObject.Find("Scroll View");
        try
        {
            obj.GetComponent<Animation>().Play("ScoreShow");
        }
        catch (System.Exception e)
        {
            Debug.Log(e + "\nScroll View not found, object name should be 'Scroll View'");
        }
    }
}

﻿////using System.Collections;
////using System.Collections.Generic;
////using System.Linq;
////using UnityEngine;

////public class MapGenerator : MonoBehaviour {

////    [SerializeField]
////    private GameObject grid;
////    [SerializeField]
////    private GameObject wall;
////    public static int X;
////    public static int Y;
////    public static List<Point> map = new List<Point>();
////    public static List<Point> walkableMap = new List<Point>();

////    public float gridSize
////    {
////        get
////        {
////            return grid.GetComponent<SpriteRenderer>().sprite.bounds.size.x;
////        }

////    }

////    // Use this for initialization
////	void Awake () {
////        GenerateMap();
////	}

////	// Update is called once per frame
////	void Update () {

////	}
////    private void GenerateMap()
////    {
////        Vector3 startPos = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height));
////        for (int y = 0; y < 10; y++)
////        {
////            for (int x = 0; x < 10; x++)
////            {

////                InstantiateGrid(x, y, startPos);
////            }
////        }
////    }
////    private void InstantiateGrid(int x,int y, Vector3 worldStart)
////    {
////        GameObject newGrid;
////        Point new_point;
////        if ( (x != 2 ) && y == 2)
////        {
////            newGrid = Instantiate(wall);
////            new_point = new Point();

////            newGrid.transform.position = new Vector2(worldStart.x + (gridSize * x), worldStart.y - (gridSize * y));
////            new_point.setProprietes(x, y, newGrid.transform.position);
////            new_point.isWalktable = false;

////        }
////        else
////        {
////            newGrid = Instantiate(grid);
////            newGrid.transform.position = new Vector2(worldStart.x + (gridSize * x), worldStart.y - (gridSize * y));
////            new_point = new Point();
////            new_point.setProprietes(x, y, newGrid.transform.position);
////            new_point.isWalktable = true;
////            newGrid.AddComponent<Location>().tilePoint = new_point;
////            map.Add(new_point);

////        }


////    }

////}
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//using UnityEngine;

//public class MazeGenerator : MonoBehaviour
//{

//    public static int step_x = 0;
//    public static List<MazeLine> lines = new List<MazeLine>();

//    private MazeLine current = null;

//    // Use this for initialization
//    void Start()
//    {

//    }

//    // Update is called once per frame
//    void Update()
//    {

//    }

//    public void MazeGen(int _x, int _y)
//    {
//        FirstLine(_x);
//        for (int y = 1; y < _y; y++)
//        {
//            current = nextLine(current, _x);
//            Retransform(current);
//            current.setPacks(current.getTiles());
//            current.setDownBoundary();

//            MazeLine line = new MazeLine(_x);
//            line = current;
//            lines.Add(line);
//        }

//    }
//    public void FirstLine(int x)
//    {
//        current = new MazeLine(x);
//        current.setTiles();
//        current.setPacks();
//        current.setDownBoundary();
//        lines.Add(current);

//    }
//    public static bool isBoundary(float threshold)
//    {
//        float rand = Random.Range(0f, 6f);
//        if (rand > threshold)
//            return true;
//        else return false;
//    }
//    private MazeLine nextLine(MazeLine line, int x)
//    {
//        MazeLine new_line = new MazeLine(x);

//        foreach (Pack p in line.packs)
//        {
//            foreach (Tile t in p.tiles)
//            {
//                Tile tile = new Tile();
//                tile.currentX = t.currentX;
//                tile.isBoundaryDown = t.isBoundaryDown;
//                tile.isBoundaryRight = t.isBoundaryRight;
//                new_line.tilelist.Add(tile);
//            }
//        }
//        return new_line;
//    }
//    private void Retransform(MazeLine line)
//    {
//        Debug.Log("BEFORE " + " B " + lines[0].tiles[0].isBoundaryRight + " D " + lines[0].tiles[0].isBoundaryDown);
//        foreach (Tile t in line.tilelist)
//            t.deleteBoundary();
//        Debug.Log("AFTER " + " B " + lines[0].tiles[0].isBoundaryRight + " D " + lines[0].tiles[0].isBoundaryDown);

//        Debug.Log("&&&&&&&" + line.tilelist.Count);
//    }
//}
//public class MazeLine
//{
//    public int currentY;
//    public Tile[] tiles;
//    public List<Tile> tilelist = new List<Tile>();
//    public List<Pack> packs = new List<Pack>();
//    public Pack currentPack = null;
//    public MazeLine(int tilesCount)
//    {
//        tiles = new Tile[tilesCount];

//    }

//    public void setTiles()
//    {
//        for (int i = 0; i < tiles.Length; i++)
//        {
//            tiles[i] = new Tile();
//            tiles[i].currentX = MazeGenerator.step_x;
//            MazeGenerator.step_x++;

//        }
//    }
//    public List<Tile> getTiles()
//    {

//        //List<Tile> tiles_in_packs = new List<Tile>();
//        //foreach(Pack p in packs)
//        //{

//        //    tiles_in_packs.AddRange(p.tiles);
//        //}

//        return tilelist;
//    }
//    public void setPacks()
//    {
//        currentPack = new Pack(tiles[0]);

//        for (int i = 0; i < tiles.Length; i++)
//        {
//            if (currentPack == null)
//                currentPack = new Pack(tiles[i]);
//            if (MazeGenerator.isBoundary(3))
//            {
//                if (i < tiles.Length - 1)
//                {

//                    tiles[i].isBoundaryRight = true;
//                    currentPack.Add(tiles[i]);
//                    packs.Add(currentPack);
//                    currentPack = null;
//                }
//                if (i == tiles.Length - 1)
//                {
//                    tiles[i].isBoundaryRight = true;
//                    currentPack.Add(tiles[i]);
//                    packs.Add(currentPack);
//                    currentPack = null;
//                    break;
//                }

//            }
//            else
//            {
//                currentPack.Add(tiles[i]);
//                if (i == tiles.Length - 1)
//                    packs.Add(currentPack);
//            }

//            //foreach(Tile t in currentPack.tiles)
//            //{
//            //    Debug.Log("---------");
//            //    Debug.Log("TILE OF " + t.currentX + "Value" + currentPack.value);
//            //}
//        }


//    }
//    public void setPacks(List<Tile> tiles)
//    {
//        Debug.Log(tiles.Count + "!!!!!!!");
//        currentPack = new Pack(tiles[0]);
//        packs.Clear();
//        Debug.Log("TILE:" + tiles[0].currentX);
//        Debug.Log("COUNTS : " + tiles.Count);
//        currentPack = new Pack(tiles[0]);

//        for (int i = 0; i < tiles.Count; i++)
//        {
//            if (currentPack == null)
//                currentPack = new Pack(tiles[i]);
//            if (MazeGenerator.isBoundary(3))
//            {
//                if (i < tiles.Count - 1)
//                {

//                    tiles[i].isBoundaryRight = true;
//                    currentPack.Add(tiles[i]);
//                    packs.Add(currentPack);
//                    currentPack = null;
//                }
//                if (i == tiles.Count - 1)
//                {
//                    tiles[i].isBoundaryRight = true;
//                    currentPack.Add(tiles[i]);
//                    packs.Add(currentPack);
//                    currentPack = null;
//                    break;
//                }

//            }
//            else
//            {
//                currentPack.Add(tiles[i]);
//                if (i == tiles.Count - 1)
//                    packs.Add(currentPack);
//            }
//        }
//    }
//    public void setDownBoundary()
//    {
//        foreach (Pack p in packs)
//        {
//            p.setBoundaryDown();

//        }
//    }
//    public void printf()
//    {
//        Debug.Log("PACK COUNT " + packs.Count);
//        foreach (Pack p in packs)
//        {
//            Debug.Log("------------");
//            Debug.Log("value " + p.value);
//            p.printTile();
//        }
//    }
//}
//public class Tile
//{
//    public int currentX;
//    public bool isBoundaryDown;
//    public bool isBoundaryRight;
//    public void deleteBoundary()
//    {
//        if (isBoundaryRight)
//            isBoundaryRight = false;
//        else if (isBoundaryDown)
//        {
//            isBoundaryDown = false;
//            currentX = MazeGenerator.step_x;
//            currentX = MazeGenerator.step_x;
//            MazeGenerator.step_x++;
//        }
//    }
//}
//public class Pack
//{

//    public int value;
//    public List<Tile> tiles = new List<Tile>();
//    public bool hasBoundaryDown;
//    public Pack(Tile t)
//    {
//        value = t.currentX;

//    }
//    public void Add(Tile t)
//    {
//        tiles.Add(t);
//    }
//    public void deleteBoundary()
//    {
//        foreach (Tile t in tiles)
//        {
//            if (t.isBoundaryRight)
//                t.isBoundaryRight = false;
//            else if (t.isBoundaryDown)
//            {
//                t.isBoundaryDown = false;
//                t.currentX = MazeGenerator.step_x;
//                value = MazeGenerator.step_x;
//                MazeGenerator.step_x++;
//            }

//        }
//    }

//    public void setBoundaryDown()
//    {
//        int index = 1;
//        for (int i = 0; i < tiles.Count; i++)
//        {
//            if (tiles.Count == 1)
//            {
//                tiles[i].isBoundaryDown = false;
//                break;
//            }
//            else if (MazeGenerator.isBoundary(3))
//            {
//                if (index < tiles.Count)
//                {
//                    tiles[i].isBoundaryDown = true;
//                    hasBoundaryDown = true;
//                    index++;
//                }
//                else
//                    tiles[i].isBoundaryDown = false;
//            }

//            else
//            {
//                if (i == tiles.Count - 1 && !hasBoundaryDown)
//                    tiles[i].isBoundaryDown = true;
//                else
//                    continue;
//            }

//        }

//    }

//    public void printTile()
//    {
//        foreach (Tile t in tiles)
//        {
//            Debug.Log("Tile x " + t.currentX + " Has right b " + t.isBoundaryRight + " has down b " + t.isBoundaryDown);

//        }
//    }


//}

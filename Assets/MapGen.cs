﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MapGen : MonoBehaviour
{
    
    [SerializeField]
    private GameObject grid;
    [SerializeField]
    private GameObject wall;
    private int Separator=1;
    private int Separator_Y=2;
    public int X=18;
    public int Y=16;
    public static List<Point> map = new List<Point>();
    public static List<Point> walkableMap = new List<Point>();
    List<string> str = new List<string>();
    List<string> str1 = null;
    private MazeGenerator generator;
    Vector3 startPos;
    
    public float gridSize
    {
        get
        {
            return grid.GetComponent<SpriteRenderer>().sprite.bounds.size.x;
        }

    }

    // Use this for initialization
    void Awake()
    {
        map.Clear();
        walkableMap.Clear();
        startPos = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height));
        generator = GetComponent<MazeGenerator>();
        generator.MazeGen((X / (Separator * 2)), (Y / Separator_Y));
        
         GenerateMap();
         CornerTile();   
    }
    void Start()
    {
        Camera.main.orthographicSize += Y / Camera.main.orthographicSize;
        //XMLMessenger mes = new XMLMessenger();
        //ScoreContainer cont = new ScoreContainer();


        //mes.xmlCreate(Application.dataPath
        //    + "\\G.xml",cont);
        // mes.xmlWrite(Application.dataPath + "\\Config.xml",cont);
        // mes.xmlRead(Application.dataPath);

        //string s = mes.xmlRead(Application.dataPath
        //    + "\\GGd.xml");
        //      Debug.Log(s);
        //XMLMessenger dd = XMLMessenger.Load(Application.dataPath + "\\Config.xml");
        //Debug.Log(dd.score+ "SCCOOORE");
    }

    
    // Update is called once per frame
    void Update()
    {

    }
    private void GenerateGrid(ref int x,int y,Tile t,int separator)
    {
        
        for (int i = 0; i < separator; i++)
        {
            InstantiateGrid(x + i, y, startPos);
            if (t.isBoundaryDown)
                InstantiatWall(x + i,  y + 1, startPos);
            else
                InstantiateGrid(x + i, y + 1, startPos);
            
        }
        
        x += separator;
        
        GenerateWalls(t,ref x, y, separator);
        
       
    }
    private void GenerateWalls(Tile t,ref int x,int y,int separator)
    {
        if (t.isBoundaryRight)
        {
            
            for (int i = 0; i < separator; i++)
            {
                InstantiatWall(x + i, y, startPos);
                InstantiatWall(x + i, y + 1, startPos);
               
            }

        }
        else
        {
            for (int i = 0; i < separator; i++)
            {
                
                InstantiateGrid(x + i, y, startPos);
                if (t.isBoundaryDown)
                    InstantiatWall(x + i, y + 1, startPos);
                else
                    InstantiateGrid(x + i, y + 1, startPos);
            }
        }
        x += separator;
    }
    private void GenerateMap()
    {
        int _y = 0;   
        for (int y = 0; y < Y/Separator_Y; y++)
        {
            
            MazeLine line;
            if (y == 0)
                line = MazeGenerator.lines[0];///tyt
            else line = MazeGenerator.lines[y];
            int X = 0;
            
            foreach (Pack p in line.packs)
            {

                foreach (Tile t in p.tiles)
                {
                    
                    GenerateGrid(ref X, _y, t, Separator);
                    
                }
            }
            _y += Separator_Y;
        }
    }
    private void setBounds(int x, int y, Vector3 worldStart)
    {
        if (x == 0 && y !=0)
            InstantiatWall(x - 1, y, startPos);
        if (y == 0 && x ==0)
        {
            InstantiatWall(x - 1, y, startPos);
            InstantiatWall(x, y - 1, startPos);
        }
        if(y == 0 && x !=0)
            InstantiatWall(x, y-1, startPos);
        if (x == X/2 && y == Y / 2)
        Camera.main.transform.position = new Vector3((startPos.x + (gridSize * x)), (startPos.y +1 - (gridSize * y)), -10f);
        if (x == X - Separator )
            InstantiatWall(x + 1, y, startPos);
            
        
        //if (x == X - Separator - 1)
        //    InstantiatWall(x+1, y, startPos);

    }
    private void InstantiateGrid(int x, int y, Vector3 worldStart)
    {
        GameObject newGrid;
        Point new_point;

        newGrid = Instantiate(grid);
        newGrid.transform.position = new Vector2(worldStart.x + (gridSize * x), worldStart.y - (gridSize * y));
        new_point = new Point();
        new_point.setProprietes(x, y, newGrid.transform.position);
        new_point.isWalktable = true;
        newGrid.GetComponent<Location>().tilePoint = new_point;
        map.Add(new_point);
        walkableMap.Add(new_point);
        setBounds(x, y, worldStart);
    }
    private void InstantiatWall(int x, int y, Vector3 worldStart)
    {
        GameObject newGrid;
        Point new_point;

        newGrid = Instantiate(wall);
        newGrid.transform.position = new Vector2(worldStart.x + (gridSize * x), worldStart.y - (gridSize * y));
        new_point = new Point();
        new_point.setProprietes(x, y, newGrid.transform.position);
        new_point.isWalktable = false;
        newGrid.GetComponent<Location>().tilePoint = new_point;
        map.Add(new_point);
        setBounds(x, y, worldStart);
    }
    private void  CornerTile()
    {
        List<Point> cornerTile = map.FindAll(t => (!t.isWalktable && (t.X > 0 && t.Y > 0) && (t.X < X - Separator && t.Y < Y -1)));
        foreach(Point p in cornerTile)
        {

            Point left = map.Find(t => t.X == p.X - 1 && t.Y == p.Y);
            Point right = map.Find(t => t.X == p.X + 1 && t.Y == p.Y);
            Point down = map.Find(t => t.X == p.X && t.Y == p.Y + 1);
            Point up = map.Find(t => t.X == p.X && t.Y == p.Y - 1);
            Point upRight = map.Find(t => t.X == p.X + 1 && t.Y == p.Y - 1);
            if(left.isWalktable && right.isWalktable && !down.isWalktable
                && up.isWalktable && !upRight.isWalktable)
            {
                Debug.Log(map.Count);
                up.isWalktable = false;
                walkableMap.Remove(up);
                
                Debug.Log(p.Position);
            }
                
            
           // Debug.Log(left.Position + "" + right.Position + "" + down.Position + "" + up.Position + "" + upRight.Position);
            
            //if ( map[p.X * ((X -Separator)*p.Y) + 1].isWalktable && map[p.X * ((X - Separator) * p.Y) - 1].isWalktable)
            //  //  !map[p.X - (X  - Separator) + 1].isWalktable && map[p.X - (X - Separator)].isWalktable)
            //    Debug.Log("###########" + p.X + " " + p.Y + " " + p.Position);
        }
    }
}

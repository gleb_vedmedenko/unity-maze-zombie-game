﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Location : MonoBehaviour {
    [SerializeField]
    private Sprite wall;
    [SerializeField]
    private Sprite grid;
    
    public Point currentLocation;
    public Point tilePoint { get { return currentLocation; } set { currentLocation = value; } }
    void Start()
    {
        if(!currentLocation.isWalktable)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = wall;
            gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
        }
        else
        {
            gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
            gameObject.GetComponent<SpriteRenderer>().sprite = grid;
        }
            
    }
}
